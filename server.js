const EXPOSED_PORT = 3000;
global.ROOT_DIR = __dirname;

const app = require('./lib/app');
const db = require('./lib/db');
const errorLogger = require('./lib/app/logger').errorLogger;
const appLogger = require('./lib/app/logger').appLogger;

app.get('/', (req, res) => {
  db.collection('todos').find().toArray((err, result) => {
    if (err) { return res.sendStatus(500); }
    appLogger.info('Root URL visited');

    return res.render('index', {todos: result});
  });
});

app.post('/todo', (req, res) => {
  db.collection('todos').insertOne(req.body, (err) => {
    if (err) { return res.sendStatus(500); }

    return res.redirect('/');
  });
});

app.use(errorLogger);
app.listen(EXPOSED_PORT);
