FROM node

EXPOSE 3000

COPY . /data/
COPY entrypoint.sh /entrypoint.sh

RUN chmod 755 /*.sh
RUN mkdir /var/log/nodejs

WORKDIR /data

RUN npm install

CMD ["npm","start"]